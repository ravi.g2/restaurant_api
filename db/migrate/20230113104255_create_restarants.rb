class CreateRestarants < ActiveRecord::Migration[6.1]
  def change
    create_table :restarants do |t|
      t.string :name
      t.string :email
      t.string :address
      t.string :contact
      t.string :location
      t.references :user

      t.timestamps
    end
  end
end
