class Item < ApplicationRecord
  belongs_to :restarant
  enum category: [:north_indian, :south_indian, :italian, :chinese]
  validates :name, presence: true
  validates :price, presence: true
  validates :category, presence: true

end
