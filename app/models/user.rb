class User < ApplicationRecord
	has_secure_password
	validates :email, uniqueness: true, format: { with: URI::MailTo::EMAIL_REGEXP } 
	validates :password_digest, presence: true
	validates :first_name, presence: true
	validates :last_name, presence: true
	validates :address, presence: true
	validates :gst_number, uniqueness: true, presence: true
	validates :account_number, uniqueness: true, length: {in: 10..15}
	validates :upi_id, presence: true, uniqueness: true
	has_one :restarant
end
