class Order < ApplicationRecord
  belongs_to :customer
  belongs_to :item
  validates :customer_id, presence: true
  validates :item_id, presence: true
  validates :quantity, presence: true
  validates :total, presence: true
  has_one :payment
  before_validation :set_total!

  def set_total!
    # byebug
    self.total = self.item.price * self.quantity.to_i
  end
end

