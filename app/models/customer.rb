class Customer < ApplicationRecord
	has_many :items, through: :orders
	has_many :orders
	has_many :payments
end
