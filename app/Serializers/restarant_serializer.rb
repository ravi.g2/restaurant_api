class RestarantSerializer
  include FastJsonapi::ObjectSerializer
  attributes :name, :email, :address, :contact
  # belongs_to :user
  # has_many :items

  attributes :image do |object, params|
    # byebug
      host = params[:host] || ''
      host + Rails.application.routes.url_helpers.rails_blob_path(object.image, only_path: true) if object.image.attached?
    end

end
