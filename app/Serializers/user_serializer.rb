class UserSerializer
  include FastJsonapi::ObjectSerializer
  attributes :first_name, :email, :gst_number, :account_number, :upi_id
  # has_one :restarant
end
