class PaymentSerializer
  include FastJsonapi::ObjectSerializer
  attributes :order_id, :total
end
