class ItemsController < ApplicationController

	before_action :set_item, only: %i[show update delete]
	before_action :confirm_login, only: %i[create update delete]


	def index
		@items = Item.all
		render json: ItemSerializer.new(@items).serializable_hash
	end

	def create
		@item = current_user.restarant.items.build(item_params)
		if @item.save
			render json: ItemSerializer.new(@item).serializable_hash
		else
			render json: @item.errors
		end
	end



	def show
		render json: ItemSerializer.new(@item).serializable_hash
	end

	def update
		@item.update(item_params)
		render json: ItemSerializer.new(@item).serializable_hash
	end

	def delete
		@item.delete
		render json: {message: "user deleted"}
	end

	private

	def item_params
		params.require(:item).permit(:name, :price, :category)
	end

	def set_items
		@item = Item.find(params[:id])
	end
end
