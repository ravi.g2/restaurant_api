class UsersController < ApplicationController

	before_action :set_user, only:%i[show update delete]
	before_action :check_owner, only:%i[update delete logout] 


	def index
		@users = User.all
		# options = {include: [:restarant]}
		render json: UserSerializer.new(@users).serializable_hash
	end

	def create
		 # byebug
		@user = User.new(user_params)
		if @user.save
			render json: UserSerializer.new(@user).serializable_hash
		else
			render json: @user.errors
		end
	end

	def show
		# options = {include: 'restarant'}
		# render json:@user.to_json(include: :restarant)
		render json: UserSerializer.new(@user).serializable_hash
	end

	def update
		if @user.update(user_params)
			render json: UserSerializer.new(@user).serializable_hash
		end
	end

	def delete
		if @user.delete
			render json:{message: "user deleted"}
		end
	end

	def login
		# byebug
		@user = User.find_by_email(params[:email]) 
			if @user&.authenticate(params[:password])
				render json: {
					token: JsonWebToken.encode(user_id: @user.id),
					data: @user, 
					restarant: @user.restarant
				}
			else
				render json:{
					message: "email or password is incorrect"
				}
			end
	end

	def logout
	end

	# def restarants
	# 	render json: User::RestarantSerializer.new(User::Restarant.all)
	# end

	private

	def set_user
		@user = User.find(params[:id])
	end

	def check_owner
		head :forbidden unless @user.id == current_user&.id
	end

	def set_login
		params.require(:user).permit(:email, :password)
	end

	def user_params
		params.require(:user).permit(:first_name, :last_name, :email, :password, :password_confirmation, :address, :gst_number, :account_number, :upi_id )
	end
end
