class CustomersController < ApplicationController

	before_action :set_customer, only: %i[show delete]

	def create
		@customer = Customer.new(customer_params)
		if @customer.save
			render json: @customer
		else
			render json: @customer.errors
		end
	end

	def get_restarant
		restarant = Restarant.all
		render json: RestarantSerializer.new(restarant).serializable_hash
	end

	def get_item
		options = {include: [:items]}
		input = Restarant.find(params[:restarant_id]).items
		render json: ItemSerializer.new(input).serializable_hash
	end

	def index
		@customers = Customer.all
		render json: CustomerSerializer.new(@customer).serializable_hash
	end

	def show
		render json: CustomerSerializer.new(@customer).serializable_hash
	end

	def delete
		@customer.delete
		render json: {message: "customer deleted"}
	end

	private

	def customer_params
		params.require(:customer).permit(:first_name, :last_name, :email, :mobile)
	end

	def set_customer
		@customer = Customer.find(params[:id])
	end
end
