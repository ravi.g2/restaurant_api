class OrdersController < ApplicationController

	before_action :set_order, only:%i[show update delete]

	def index
		@orders = Order.all
		render json: OrderSerializer.new(@orders).serializable_hash
	end

	def create
		@order = Order.new(order_params)
		# byebug
		if @order.save
			render json: OrderSerializer.new(@order).serializable_hash
		else
			render json: @order.errors
		end
	end

	def show
		render json: OrderSerializer.new(@order).serializable_hash
	end

	def update
		if @order.update(order_params)
			render json: @order
		else
			render json: {message: "login first"}
		end
	end

	def delete
		@user.delete
		render json: {message: "order cancelled"}
	end

	private

	def order_params
		params.require(:order).permit(:quantity, :customer_id, :item_id, :total)
	end

	def set_order
		@order = Order.find(params[:id])
	end
end
