class RestarantsController < ApplicationController
	before_action :confirm_login
	before_action :set_restarant, only: %i[show update delete]
	before_action :confirm_owner, only: %i[show update delete]

	def index
		@restarants = current_user.restarant
		# options = {include: [:user]}
		render json: RestarantSerializer.new(@restarants).serializable_hash
	end

	def show
		# options = {include: [:user]}
		render json: RestarantSerializer.new(@restarant,{ params: { host: request.protocol + request.host_with_port}}).serializable_hash
	end

	def create
		@restarant = Restarant.new(restarant_params)
		# byebug
		# user_id = current_user.id
		@restarant.user_id = current_user.id
		if @restarant.save

			# list = @restarant.images.map {|image| url_for(image)}
			render json: RestarantSerializer.new(@restarant, {params: { host: request.protocol + request.host_with_port}}).serializable_hash
		else
			render json: @restarant.errors
		end
	end

	def update
		if @restarant.update(restarant_params)
			# list = @restarant.imageurl_for(image)}

			render json: RestarantSeializer.new(@restarant, {params: { host: request.protocol + request.host_with_port}}).serializable_hash
		else
			render json: {message: "login first"}
		end
	end

	def delete
		@restarant.delete
		render json: {message: "restarant deleted by the user"}
	end

	# def users
	# 	render json: Restarant::UserSerializer.new(Restarant::User.all)
	# end

	private

	def confirm_owner
		render json:{message: 'user not found'} unless @restarant.user_id == current_user&.id
	end

	def restarant_params
		params.require(:restarant).permit(:name, :email, :address, :contact, :location, :image)
	end

	def set_restarant
		@restarant = current_user.restarant
	end

	
end
