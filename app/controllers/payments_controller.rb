class PaymentsController < ApplicationController

	before_action :set_payment, only: %i[show]
	# before_action :confirm_login, only: %i[index create show]

	def index
		@payments = Payment.all
		render json: PaymentSerializer.new(@payments).serializable_hash
	end

	def create
		@payment = Payment.new(payment_params)
			if @payment.order.total == @payment.total
				if @payment.save
				    render json: {message: "payment successful"}
				else 
					render json: @payment.errors
				end
			else
				render json: {message: "please check your payment details"}
			end
	end

	def show
		render json: PaymentSerializer.new(@payment).serializable_hash
	end

	private

	def payment_params
		params.require(:payment).permit(:customer_id, :order_id, :total)
	end

	def set_payment
		@payment = Payment.find(params[:id])
	end

end
